#It should contain the code that you use to make the HTTP requests to Pexels and Open Weather Map.
#Any code in here should return Python dictionaries to be used by your view code.
#To use the keys from the events/keys.py file, add this as one of your imports:
#Then, use those variables in your code when you make your requests.

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}, US",
        "limit": 1,#how many locations w weather data come back
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(url, params=params)
    content = response.json()
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError): #If no lat or lon, or if response is empty list
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    content = response.json()
    return {
        "description": content["weather"][0]["description"],#dict in a list in a dict
        "temp": content["main"]["temp"],
    }

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
